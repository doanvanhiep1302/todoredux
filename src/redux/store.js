import {createStore, combineReducers} from 'redux';
import { listReducer } from './reducers/listReducers';

const reducer = combineReducers({
    todoItems : listReducer
})


const store = createStore(
    reducer
)

export default store;