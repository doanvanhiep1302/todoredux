import { LIST_ADD , LIST_DELETE,LIST_COMPLETE,LIST_DATA, NAME} from "../../constants/ListConstants";

export const listReducer = (state = {todoList : [], repeat : false},action) =>{
    console.log(state.todoList,action.payload)
    switch(action.type){
        case LIST_ADD: 
            const newList =action.payload;
           
            return{
                ...state,
                repeat: false,
                todoList: [...state.todoList, newList]
            }
           
            
            case NAME: 
            return{
                ...state,
                todoList: [...state.todoList],
                repeat : true
            }
        case LIST_DATA: 
        console.log(action.payload)
            return{
                ...state,
                todoList: [ ...action.payload]
            }
        case LIST_DELETE: 
        const data = state.todoList.filter(x =>x.name !== action.payload)
            return{
                ...state,
                todoList: data
            }
        case LIST_COMPLETE: 
            const index = state.todoList.findIndex(x => x.id === action.payload)
            const setData = state.todoList[index]
            const newData = {...setData, complete: !setData.complete}
            const dataAll = [...state.todoList]
            dataAll[index] = newData

            return{
                ...state,
                todoList : [...dataAll]
            }
        // case LIST_REMOVE: 
        //     const uncompleteNote = state.todoList.find(x => x.name === action.payload.name)
        //     return{
        //         ...state,
        //         todoList : state.todoList.map((x) => x.name === uncompleteNote.name ? action.payload : x)
        //     }
        default:

            return state
    }
}