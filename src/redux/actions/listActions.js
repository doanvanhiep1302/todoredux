import { LIST_ADD, LIST_DELETE} from "../../constants/ListConstants";


export const addList =(name) => async (dispatch) =>{
    
          dispatch({
            type: LIST_ADD,
            payload:{
                name: name,
                complete: false
            } 
        })
        
    }


export const deleteList = (id) => async (dispatch,getState) =>{
    console.log(id)
    dispatch({
        type: LIST_DELETE,
        payload: id
    })
}


// export const removeList =(name) => async (dispatch, getState) =>{
//     dispatch({
//         type: LIST_REMOVE,
//         payload:{
//             name: name,
//             complete: true
//         } 
//     })
//     // localStorage.setItem('listItems', JSON.stringify(getState().todoItems.todoList))
// }

