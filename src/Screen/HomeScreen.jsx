import React from 'react';
import {Card} from 'react-bootstrap';
import TodoListForm from '../Component/TodoListForm';
import TodoListItem from '../Component/TodoListItem';

const HomeScreen = () => {
  return (
    <div>
        <h1 className="text-center">TodoList Redux</h1>
        <Card>
            <Card.Header className="text-center">
                A todolist using React,Redux
            </Card.Header>
            <TodoListForm />
        </Card>
        <TodoListItem />
    </div>
  )
}

export default HomeScreen