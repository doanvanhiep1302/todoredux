import React,{useEffect, useState} from 'react';
import {ListGroup, Row, Col} from 'react-bootstrap';
import {FaTrash} from 'react-icons/fa';
import {AiFillCheckSquare} from 'react-icons/ai';
import {RiEraserLine} from 'react-icons/ri';
import {useSelector, useDispatch} from 'react-redux';
import Message from './Message';
import {deleteList ,completeList,removeList, listData} from '../redux/actions/listActions'
import { LIST_COMPLETE, LIST_DATA, LIST_DELETE } from '../constants/ListConstants';

const userApi ="http://localhost:3004/user";


const TodoListItem = (callback) => {
  const dispatch = useDispatch()
  const data = useSelector((state) => state.todoItems)
  console.log(data.todoList)


  useEffect(()=> {
    
    fetch(userApi)
  .then(response => response.json())
  .then(data => {
    console.log(data)
    dispatch({
      type: LIST_DATA,
      payload: data
  })
  });

  },[])


 

  const handleDelete = (id, name) =>{
    console.log("log id:", id)
    dispatch({
      type: LIST_DELETE,
      payload: name
  })
    var options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch(userApi + "/" + id, options)
  }

  const handleComplete = (name) =>{
    // dispatch(completeList(name))
  }
  const handleIncomplete = (id,  complete, name) =>{
   console.log(complete)

   dispatch({
    type: LIST_COMPLETE,
    payload: id
})
    
    var options = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        complete: !complete,
        name: name
    }),
  };
    fetch(userApi + "/" + id, options)
  }
  
  
 
    return (
      <div>
      {data.repeat && <Message variant ="danger">Tên bạn nhập đã trùng.Mời nhập tên khác !</Message>}
          <ListGroup>
          {
            data.todoList.length > 0 && data.todoList.map((list) => (
              <ListGroup.Item  key={list.id}>
                  <Row>
                      <Col md ={8} xs= {8}
                      > 
                      {
                        list.complete ===true ?
                        (
                          <p><del>{list.name}</del></p>
                        )
                        :
                        (
                          <p>{list.name}</p>
                        )
                      }
                      
                      </Col>
  
                      <Col md ={2} xs= {2} >
                      {
                        list.complete ===true ?
                        (
                          <AiFillCheckSquare onClick ={() => handleIncomplete(list.id, list.complete)}/>
                        )
                        :
                        (
                          <RiEraserLine onClick ={() => handleIncomplete(list.id, list.complete, list.name)}/> 
                        )
                      }
                      </Col>
  
                      <Col md ={2} xs= {2}>
                      <FaTrash onClick ={() => handleDelete(list.id, list.name)}/>
                      </Col>
                  </Row>
              </ListGroup.Item>
            )
            ) 
          }
              
          </ListGroup>
      </div>
    )
  }
  
 
export default TodoListItem