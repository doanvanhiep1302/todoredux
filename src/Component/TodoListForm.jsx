import React,{useState} from 'react';
import { Form, Row, Col, Button } from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';
import { LIST_ADD, NAME } from '../constants/ListConstants';
import {addList} from '../redux/actions/listActions'

const userApi ="http://localhost:3004/user";
const TodoListForm = () => {
    const dispatch =useDispatch()
    const [list,setList] = useState('');
    const data = useSelector((state) => state.todoItems)
    console.log(data)

    const submitHandle = (e) => {
        const id =  Math.random() +  Math.random()
        e.preventDefault();
        const checkName = data.todoList.find(x => x.name === list)
        console.log(checkName)
        if (checkName) {
            dispatch({
                type: NAME,
                });
            } else {
                dispatch({
                    type: LIST_ADD,
                    payload:{
                        name: list,
                        complete: false,
                        id: id
                    } });
            setList('');
    
             const options = {
               method: "POST",
               headers: {
                 "Content-Type": "application/json",
               },
               redirect: "follow",
               body: JSON.stringify({
                name: list,
                complete: false,
                id: id
                
            }),
             };
    
             fetch(userApi, options)
               .then(function (response) {       
                 response.json();
                 // dispatch()
               })

        }
        
        }
        
        // console.log("Hello my name is Hiep !")
    
    return (
        <Form className='mx-2 my-2' onSubmit={submitHandle}>
            <Form.Group controlId='inputList'>
                <Row>
                    <Col md={8}>
                        <Form.Control 
                        type="text" 
                        placeholder="Nhập vào đây..."
                        required
                        value ={list}
                        onChange ={(e) => setList(e.target.value)}
                        />
                    </Col>
                    <Col md={4}>
                        <Button 
                        variant="primary"
                         type="submit">
                           Thêm
                        </Button>
                    </Col>
                </Row>
            </Form.Group>
        </Form>
    )
}

export default TodoListForm